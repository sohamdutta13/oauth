package com.date.iitians.oauth;

/**
 * Created by liveongo on 31/5/16.
 */
public interface HelperCallback {

    void onSuccess(String accessToken);

    void onFailure();
}
