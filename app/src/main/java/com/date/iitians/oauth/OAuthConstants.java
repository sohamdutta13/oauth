package com.date.iitians.oauth;

/**
 * Created by liveongo on 30/5/16.
 */
public class OAuthConstants {
    //Platforms
    public static final String LINKEDIN = "linkedin";
    public static final String TWITTER = "twitter";
    public static final String INSTAGRAM = "instagram";
    public static final String FOURSQUARE = "foursquare";

    public static final String QUESTION_MARK = "?";
    public static final String AMPERSAND = "&";
    public static final String EQUALS = "=";


    //Linkedin URLS
    public static final String AUTHORIZATION_URL = "https://www.linkedin.com/uas/oauth2/authorization";
    public static final String ACCESS_TOKEN_URL = "https://www.linkedin.com/uas/oauth2/accessToken";
    public static final String SECRET_KEY_PARAM = "client_secret";
    public static final String RESPONSE_TYPE_PARAM = "response_type";
    public static final String GRANT_TYPE_PARAM = "grant_type";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String RESPONSE_TYPE_VALUE ="code";
    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String STATE_PARAM = "state";
    public static final String REDIRECT_URI_PARAM = "redirect_uri";

    // Instagram URLS
/*
Client ID 12392fa89ec74b90846e259a59dea947

Client Secret 29abe23c3cf640b4ab4c1ece7e88ae77
* */
    public static final String INSTAGRAM_AUTHORIZATION_URL = "https://api.instagram.com/oauth/authorize";
    public static final String INSTAGRAM_ACCESS_TOKEN_URL = "https://api.instagram.com/oauth/access_token";
    public static final String INSTAGRAM_SECRET_KEY_PARAM = "client_secret";
    public static final String INSTAGRAM_RESPONSE_TYPE_PARAM = "response_type";
    public static final String INSTAGRAM_GRANT_TYPE_PARAM = "grant_type";
    public static final String INSTAGRAM_GRANT_TYPE = "authorization_code";
    public static final String INSTAGRAM_RESPONSE_TYPE_VALUE ="token";
    public static final String INSTAGRAM_ACCESS_TOKEN_RESPONSE_TYPE_PARAM ="access_token";
    public static final String INSTAGRAM_CLIENT_ID_PARAM = "client_id";
    public static final String INSTAGRAM_STATE_PARAM = "state";
    public static final String INSTAGRAM_REDIRECT_URI_PARAM = "redirect_uri";


    //Foursquare constants

    public static final String FOURSQUARE_AUTHORIZATION_URL = "https://foursquare.com/oauth2/authenticate";
    public static final String FOURSQUARE_RESPONSE_TYPE_VALUE ="token";



    //TWITTER

    public static final String TWITTER_REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token";//https://api.twitter.com/oauth/request_token?oauth_consumer_key=tzJGgnAE0gc0E58sc8qEX2gMF&oauth_nonce=f348aae1a1724e87800de190329976af&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1466588992&oauth_version=1.0&oauth_signature=TQCFjCtYP6BqED5qRa7bDvcZo8A%3D
    // response will look like this => oauth_token=vGic5wAAAAAAum2hAAABVXeYs4I&oauth_token_secret=hr7ciFEX3HsITUAoICrhaHeXBRxElpRj&oauth_callback_confirmed=true
    public static final String TWITTER_AUTH_TOKEN_URL = "https://api.twitter.com/oauth/authorize";//https://api.twitter.com/oauth/authorize?oauth_token=81Z_bQAAAAAAum2hAAABVXeQgfk
    // use the auth token from the response which will look like =>  http://www.dateiitians.com/redirection?oauth_token=vGic5wAAAAAAum2hAAABVXeYs4I&oauth_verifier=8dyr86bylcwItXFHnmoYDAhjuXup4zk9
    public static final String TWITTER_ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";//https://api.twitter.com/oauth/access_token?oauth_token=vGic5wAAAAAAum2hAAABVXeYs4I&oauth_verifier=8dyr86bylcwItXFHnmoYDAhjuXup4zk9
    // response will look like this => oauth_token=361865962-l7dq29fwgPgaEt24swrZZ2bHlpwMJ7h5Nym4B3H4&oauth_token_secret=4hhs1oxTxZpITb7Nyflt95xEgI0ulvNpRrJ1z19jlm0OH&user_id=361865962&screen_name=dateIITians&x_auth_expires=0

}
