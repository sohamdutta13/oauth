package com.date.iitians.oauth;

/**
 * Created by liveongo on 31/5/16.
 */
public class OAuthUtils {

    /**
     * Method that generates the url for get the access token from the Service
     * @return Url
     */
    public static String getAccessTokenUrl(String authorizationToken,OAuthHelper helper){
        return OAuthConstants.ACCESS_TOKEN_URL
                +OAuthConstants.QUESTION_MARK
                +OAuthConstants.GRANT_TYPE_PARAM+OAuthConstants.EQUALS+OAuthConstants.GRANT_TYPE
                +OAuthConstants.AMPERSAND
                +OAuthConstants.RESPONSE_TYPE_VALUE+OAuthConstants.EQUALS+authorizationToken
                +OAuthConstants.AMPERSAND
                +OAuthConstants.CLIENT_ID_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_API_KEY
                +OAuthConstants.AMPERSAND
                +OAuthConstants.REDIRECT_URI_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_REDIRECT_URI
                +OAuthConstants.AMPERSAND
                +OAuthConstants.SECRET_KEY_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_SECRET_KEY;
    }

    /**
     * Method that generates the url for get the authorization token from the Service
     * @return Url
     */
    public static String getLinkedInAuthorizationUrl(OAuthHelper helper){
        return OAuthConstants.AUTHORIZATION_URL
                +OAuthConstants.QUESTION_MARK+OAuthConstants.RESPONSE_TYPE_PARAM+OAuthConstants.EQUALS+OAuthConstants.RESPONSE_TYPE_VALUE
                +OAuthConstants.AMPERSAND+OAuthConstants.CLIENT_ID_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_API_KEY
                +OAuthConstants.AMPERSAND+OAuthConstants.STATE_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_STATE
                +OAuthConstants.AMPERSAND+OAuthConstants.REDIRECT_URI_PARAM+OAuthConstants.EQUALS+helper.LINKEDIN_REDIRECT_URI;
    }

    /**
     * Method that generates the url for get the authorization token from the Service
     * @return Url
     */
    public static String getInstagramAuthorizationUrl(OAuthHelper helper){
        return OAuthConstants.INSTAGRAM_AUTHORIZATION_URL
                +OAuthConstants.QUESTION_MARK+OAuthConstants.CLIENT_ID_PARAM+OAuthConstants.EQUALS+helper.INSTAGRAM_CLIENT_KEY
                +OAuthConstants.AMPERSAND+OAuthConstants.REDIRECT_URI_PARAM+OAuthConstants.EQUALS+helper.INSTAGRAM_REDIRECT_URI
                +OAuthConstants.AMPERSAND+OAuthConstants.INSTAGRAM_RESPONSE_TYPE_PARAM+OAuthConstants.EQUALS+OAuthConstants.INSTAGRAM_RESPONSE_TYPE_VALUE;
    }

    /**
     * Method that generates the url for get the authorization token from the Service
     * @return Url
     */
    public static String getFoursquareAuthorizationUrl(OAuthHelper helper){
        return OAuthConstants.FOURSQUARE_AUTHORIZATION_URL
                +OAuthConstants.QUESTION_MARK+OAuthConstants.CLIENT_ID_PARAM+OAuthConstants.EQUALS+helper.FOURSQUARE_CLIENT_KEY
                +OAuthConstants.AMPERSAND+OAuthConstants.REDIRECT_URI_PARAM+OAuthConstants.EQUALS+helper.FOURSQUARE_REDIRECT_URI
                +OAuthConstants.AMPERSAND+OAuthConstants.INSTAGRAM_RESPONSE_TYPE_PARAM+OAuthConstants.EQUALS+OAuthConstants.FOURSQUARE_RESPONSE_TYPE_VALUE;
    }

    /**
     * Method that generates the url for get the authorization token from the Service
     * @return Url
     */
    public static String getTwitterAuthorizationUrl(OAuthHelper helper){
        return OAuthConstants.TWITTER_AUTH_TOKEN_URL
                +OAuthConstants.QUESTION_MARK+helper.twitterRequestToken;
    }

    /**
     * Method that generates the url for get the access token for instagram
     * @return Url
     */
    /*public static String getInstagramAccessTokenUrl(String authorizationToken,OAuthHelper helper){
        return OAuthConstants.INSTAGRAM_ACCESS_TOKEN_URL
                +OAuthConstants.QUESTION_MARK
                +OAuthConstants.GRANT_TYPE_PARAM+OAuthConstants.EQUALS+OAuthConstants.GRANT_TYPE
                +OAuthConstants.AMPERSAND
                +OAuthConstants.INSTAGRAM_RESPONSE_TYPE_VALUE+OAuthConstants.EQUALS+authorizationToken
                +OAuthConstants.AMPERSAND
                +OAuthConstants.CLIENT_ID_PARAM+OAuthConstants.EQUALS+helper.INSTAGRAM_CLIENT_KEY
                +OAuthConstants.AMPERSAND
                +OAuthConstants.REDIRECT_URI_PARAM+OAuthConstants.EQUALS+helper.INSTAGRAM_REDIRECT_URI
                +OAuthConstants.AMPERSAND
                +OAuthConstants.SECRET_KEY_PARAM+OAuthConstants.EQUALS+helper.INSTAGRAM_SECRET_KEY;
    }*/
}
