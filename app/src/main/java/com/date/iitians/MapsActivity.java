package com.date.iitians;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.date.iitians.oauth.HelperCallback;
import com.date.iitians.oauth.OAuthConstants;
import com.date.iitians.oauth.OAuthHelper;

public class MapsActivity extends Activity {
    Button btLinkedin,btInstagram,btFoursquare,btTwitter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauthhelper);
        btLinkedin = (Button) findViewById(R.id.linkdin);

        btLinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OAuthHelper oAuthHelper = new OAuthHelper(OAuthConstants.LINKEDIN, MapsActivity.this);
                oAuthHelper.setLinkedinData("75zerrjm3hq006", "OXLJvHZT8mwHLmjH", "asdfghjkl", "http://www.dateiitians.com/");
                oAuthHelper.startOAuth("LinkedIn Login");
                oAuthHelper.helperCallback = new HelperCallback() {
                    @Override
                    public void onSuccess(String accessToken) {
                        Log.e("LOGGER", accessToken);
                        Toast.makeText(MapsActivity.this, "Success", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure() {

                    }
                };

            }
        });

        btInstagram = (Button) findViewById(R.id.instagram);

        btInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OAuthHelper oAuthHelper = new OAuthHelper(OAuthConstants.INSTAGRAM, MapsActivity.this);
                oAuthHelper.setInstagramData("12392fa89ec74b90846e259a59dea947", "29abe23c3cf640b4ab4c1ece7e88ae77", "http://www.dateiitians.com");
                oAuthHelper.startOAuth("INSTAGRAM Login");
                oAuthHelper.helperCallback = new HelperCallback() {
                    @Override
                    public void onSuccess(String accessToken) {
                        Log.e("LOGGER",accessToken);
                        Toast.makeText(MapsActivity.this,"Success",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure() {

                    }
                };

            }
        });

        btFoursquare = (Button) findViewById(R.id.foursquare);

        btFoursquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OAuthHelper oAuthHelper = new OAuthHelper(OAuthConstants.FOURSQUARE, MapsActivity.this);
                oAuthHelper.setFoursquareData("H1AZJ05ACJTV3VROQ4Y5KNLJPZLAF3JVE0DVVWN5O34KARMC", "1PX3GM4DZZ02H3XNOQN41OUVQOYKC20EZQGYZPZ2XKRXSPRM", "http://www.dateiitians.com");
                oAuthHelper.startOAuth("Foursquare Login");
                oAuthHelper.helperCallback = new HelperCallback() {
                    @Override
                    public void onSuccess(String accessToken) {
                        Log.e("LOGGER",accessToken);
                        Toast.makeText(MapsActivity.this,"Success",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure() {

                    }
                };

            }
        });

        btTwitter = (Button) findViewById(R.id.twitter);

        btTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OAuthHelper oAuthHelper = new OAuthHelper(OAuthConstants.TWITTER, MapsActivity.this);
                oAuthHelper.setTwitterData("tzJGgnAE0gc0E58sc8qEX2gMF", "nrbkT0zE7truHMPWjMVUwOcb7zmyezpda82io7A87RGfD4Emj8","http://www.dateiitians.com/redirection");
                oAuthHelper.startOAuth("Twitter Login");
                oAuthHelper.helperCallback = new HelperCallback() {
                    @Override
                    public void onSuccess(String accessToken) {
                        Log.e("LOGGER",accessToken);
                        Toast.makeText(MapsActivity.this,"Success",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure() {

                    }
                };

            }
        });
    }


}
